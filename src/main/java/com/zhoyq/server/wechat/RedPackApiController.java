/*
 *   Copyright (c) 2018. 刘路 All rights reserved
 *   版权所有 刘路 并保留所有权利 2018.
 *   ===============================================================
 *   这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 *   使用。不允许对程序代码以任何形式任何目的的再发布。如果项目发布携带作者
 *   认可的特殊 LICENSE 则按照 LICENSE 执行，废除上面内容。请保留原作者信息。
 *   ================================================================
 *   刘路（feedback@zhoyq.com）于 2018. 创建
 *   http://zhoyq.com
 */

package com.zhoyq.server.wechat;

import com.jfinal.core.Controller;
import com.jfinal.kit.StrKit;
import com.jfinal.weixin.sdk.api.RedPackApi;
import com.jfinal.weixin.sdk.kit.IpKit;
import com.jfinal.weixin.sdk.kit.PaymentKit;

import java.util.HashMap;
import java.util.Map;

/**
 * 微信红包demo
 * @author osc余书慧
 */
public class RedPackApiController extends Controller {
    // 商户相关资料
    private static String wxappid = "";
    // 微信支付分配的商户号
    private static String partner = "";
    private static String sendName = "";
    //API密钥
    private static String paternerKey = "";
    //微信证书路径
    private static String certPath = "";

    public void send() {
        // 接受红包的用户用户在wxappid下的openid
        String reOpenid = "";
        // 商户订单号
        String mchBillno = System.currentTimeMillis() + "";
        String ip = IpKit.getRealIp(getRequest());

        Map<String, String> params = new HashMap<String, String>();
        // 随机字符串
        params.put("nonce_str", System.currentTimeMillis() / 1000 + "");
        // 商户订单号
        params.put("mch_billno", mchBillno);
        // 商户号
        params.put("mch_id", partner);
        // 公众账号ID
        params.put("wxappid", wxappid);
        // 商户名称
        params.put("send_name", sendName);
        // 用户OPENID
        params.put("re_openid", reOpenid);
        // 付款现金(单位分)
        params.put("total_amount", "100");
        // 红包发放总人数
        params.put("total_num", "1");
        // 红包祝福语
        params.put("wishing", "恭喜您....");
        // 终端IP
        params.put("client_ip", ip);
        // 活动名称
        params.put("act_name", "床垫睡眠日活动");
        // 备注
        params.put("remark", "新年新气象");
        //创建签名
        String sign = PaymentKit.createSign(params, paternerKey);
        params.put("sign", sign);

        String xmlResult = RedPackApi.sendRedPack(params, certPath, partner);
        Map<String, String> result = PaymentKit.xmlToMap(xmlResult);
        System.out.println(result);
        //业务结果
        String result_code = result.get("result_code");
        //此字段是通信标识，非交易标识，交易是否成功需要查看result_code来判断
        String return_code = result.get("return_code");
        //
        if (StrKit.isBlank(result_code) || !"SUCCESS".equals(result_code)) {
            System.out.println("发送成功");
        } else {
            System.out.println("发送失败");
        }
        renderJson(result);
    }

    public void query() {
        Map<String, String> params = new HashMap<String, String>();
        // 随机字符串
        params.put("nonce_str", System.currentTimeMillis() / 1000 + "");
        // 商户订单号
        params.put("mch_billno", "20160227083703842100294140");
        // 商户号
        params.put("mch_id", partner);
        // 公众账号ID
        params.put("appid", wxappid);
        params.put("bill_type", "MCHT");
        //创建签名
        String sign = PaymentKit.createSign(params, paternerKey);
        params.put("sign", sign);

        String xmlResult = RedPackApi.getHbInfo(params, certPath, partner);
        Map<String, String> result = PaymentKit.xmlToMap(xmlResult);
        System.out.println(result);
        renderJson(result);
    }

}
