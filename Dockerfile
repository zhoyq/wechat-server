FROM tomcat:8.5.34-jre8-alpine
MAINTAINER zhoyq <feedback@zhoyq.com>
RUN rm -rf /usr/local/tomcat/webapps && mkdir /usr/local/tomcat/webapps
COPY ./target/wechat-server.war /usr/local/tomcat/webapps/ROOT.war
EXPOSE 8080
VOLUME ["/etc/a_little_config_pro.txt"]
