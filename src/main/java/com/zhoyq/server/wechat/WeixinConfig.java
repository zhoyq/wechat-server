/*
 *   Copyright (c) 2018. 刘路 All rights reserved
 *   版权所有 刘路 并保留所有权利 2018.
 *   ===============================================================
 *   这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 *   使用。不允许对程序代码以任何形式任何目的的再发布。如果项目发布携带作者
 *   认可的特殊 LICENSE 则按照 LICENSE 执行，废除上面内容。请保留原作者信息。
 *   ================================================================
 *   刘路（feedback@zhoyq.com）于 2018. 创建
 *   http://zhoyq.com
 */

package com.zhoyq.server.wechat;

import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;
import com.jfinal.kit.PropKit;
import com.jfinal.render.ViewType;
import com.jfinal.template.Engine;
import com.jfinal.weixin.sdk.api.ApiConfig;
import com.jfinal.weixin.sdk.api.ApiConfigKit;
import com.jfinal.wxaapp.WxaConfig;
import com.jfinal.wxaapp.WxaConfigKit;

import java.io.File;

public class WeixinConfig extends JFinalConfig {
    // 本地开发模式
    private boolean isLocalDev = false;
    
    /**
     * 如果生产环境配置文件存在，则优先加载该配置，否则加载开发环境配置文件
     * @param pro 生产环境配置文件
     * @param dev 开发环境配置文件
     */
    public void loadProp(String pro, String dev) {
        try {
            File file = new File(pro);
            PropKit.use(file);
        } catch (Exception e) {
            PropKit.use(dev);
            isLocalDev = true;
        }
    }

    @Override
    public void configConstant(Constants me) {
        loadProp("/etc/a_little_config_pro.txt", "a_little_config.txt");
        me.setDevMode(PropKit.getBoolean("devMode", false));
        me.setViewType(ViewType.JFINAL_TEMPLATE);
        // ApiConfigKit 设为开发模式可以在开发阶段输出请求交互的 xml 与 json 数据
        ApiConfigKit.setDevMode(me.getDevMode());
    }
    @Override
    public void configRoute(Routes me) {
        me.add("/",IndexController.class,"/WEB-INF/temp/");
        me.add("/msg", WeixinMsgController.class);
//        me.add("/api", WeixinApiController.class, "/api");
//        me.add("/pay", WeixinPayController.class);
//        me.add("/wxa/user", WxaUserApiController.class);
    }
    @Override
    public void configPlugin(Plugins me) {  }
    @Override
    public void configInterceptor(Interceptors me) { }
    @Override
    public void configHandler(Handlers me) { }
    @Override
    public void afterJFinalStart() {

        ApiConfig ac = new ApiConfig();
        // 配置微信 API 相关参数
        ac.setToken(PropKit.get("token"));
        ac.setAppId(PropKit.get("appId"));
        ac.setAppSecret(PropKit.get("appSecret"));

        ac.setEncryptMessage(PropKit.getBoolean("encryptMessage", false));
        ac.setEncodingAesKey(PropKit.get("encodingAesKey", "setting it in config file"));

        ApiConfigKit.putApiConfig(ac);

        WxaConfig wc = new WxaConfig();
        wc.setAppId(PropKit.get("appId"));
        wc.setAppSecret(PropKit.get("appSecret"));
        WxaConfigKit.setWxaConfig(wc);
    }

    public static void main(String[] args) {
        JFinal.start("src/main/webapp", 8080, "/", 5);
    }

	@Override
	public void configEngine(Engine engine) { }
}
